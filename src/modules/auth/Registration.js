import React, {useState} from 'react';
import {Link} from 'react-router-dom'
import {authRequirements} from '../common/constants'
import Title from '../common/components/Title'
import Input from '../common/hookforms/Input'
import Email from '../common/hookforms/Email'
import Submit from '../common/hookforms/Submit'
import Password from '../common/hookforms/Password'
import CenteredScreen from '../layout/CenteredScreen'
import {Form} from 'react-bootstrap'
import useForm from 'react-hook-form'
import styled from 'styled-components'
import AuthBanner from './AuthBanner'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button"
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Box from '@material-ui/core/Box';
import Checkbox from '@material-ui/core/Checkbox';
import { makeStyles } from '@material-ui/core/styles';
import { faBold } from '@fortawesome/pro-light-svg-icons';
import register from '/Users/mustafamerchant/Desktop/developer-react-test/src/modules/auth/Registration.js'
const Wrapper = styled(CenteredScreen)`
  .auth-banner{
    width:325px;
    margin-bottom:15px;
    @media (min-width: 768px) {
        width:400px;
    }
  }
  form{
    width:300px;
    text-align:left;
    margin:0 auto 15px;
    .submit{
      margin-top:42px;
      text-align:center;
    }
  }
`


const Registration = ({location, history})=> {
  const [open, setOpen] = useState(false);
  const form = useForm()
  const onSubmit = (val) => setOpen(true);

  const [show, setShow] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };



  
  const [scroll, setScroll] = React.useState('paper');



  const descriptionElementRef = React.useRef(null);
  React.useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open]);



  function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  
  const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    heading: {
      fontSize: theme.typography.pxToRem(20),
      fontWeight: theme.typography.fontWeightRegular,
    },
    modal:{
      position:'absolute',
      top:'10%',
      left:'10%',
      overflow:'scroll',
      height:'100%',
      maxHeight: 500,
      display:'block'
    },
    header: {
      padding: '16px 0',
      borderBottom: '1px solid darkgrey'
    },
    content: {
      padding: 12,
      overflow: 'scroll'
    }
  }));


  const classes = useStyles();

  const onlyAlpha = /^\w+$/
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const [checked, setChecked] = React.useState(false);

  const handleChange_checkbox = (event) => {
    setChecked(event.target.checked);
  };


  return (
  <>
    <Wrapper className="screen-login">
    <Title title="Log in"/>
    <div className="container-fluid">
      <AuthBanner/>
      <Form horizontal onSubmit={form.handleSubmit(onSubmit)}>
        <Input name="username" form={form} label="Username"
        minLength={authRequirements.USERNAME_MIN} maxLength={authRequirements.USERNAME_MAX}
        pattern={onlyAlpha} patternLabel='Must be only a-z 0-9' required/>
        <Email name="email" form={form} label="Email" required/>
        <Password name="password" form={form} label="Password" required/>
        <p>By clicking Create Account you agree to our <Link to="/terms">Terms</Link> and <Link to="/privacy">Privacy Policy</Link></p>
        <Submit className="submit" label="Create account" form={form} primary/>


        
        
      </Form>
      
      <p>Already have an account? <Link to="login">Sign in now</Link></p>
    </div>
  </Wrapper>

  <Dialog
        open={open}
        onClose={handleClose}
        scroll='paper'
        style={{width: '800px', height: '800px' ,marginLeft: '30%', backgroundColor: 'transparent'}}
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
<DialogTitle
    disableTypography
    id="alert-dialog-title"
    style={{ backgroundColor: 'white', color: 'black' }}
>
    <Typography align="center" variant="h4">Terms and Privacy Policy</Typography>
</DialogTitle>

        <div className={classes.root}>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>Statement of Intent</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography className= {classes.heading}>
            We want you to know exactly how our services work and why we need your details. Reviewing our policy will help
            you continue using the app with peace of mind.
          </Typography>
        </AccordionDetails>
      </Accordion>
      </div>
      <AppBar position="static">
  <Tabs value={value} onChange={handleChange} >
    <Tab className = {classes.heading} label="Terms of Use" />
    <Tab className = {classes.heading} label="Privacy Policy"  />
  </Tabs>
</AppBar>




        <DialogContent dividers={scroll === 'paper'}>
        <TabPanel value={value} index={0}>
          
          <b className = {classes.heading}>1. What personal information we collect</b><br></br>
        
          <br></br>
        
        Depending on how you engage with Cogniss Services, we collect different kinds of information from or about you.
        
        <br></br>
        <br></br>
        Account and profile information.
        <br></br>
        <br></br>
        We collect information about you when you register for an account, create or modify your profile, 
        set preferences, or signup for our Services. This includes your name, email address, password, and 
        in some cases your date of birth and gender.
        <br></br>
        <br></br>
        Information you provide to us
        <br></br>
        <br></br>
        
        We collect and store any content that you create, post, send, and share to our Services. This content includes any comments you post, emails and other communications that you send to us, and information you provide by responding to surveys, submitting a form or participating in promotions. We also track how you engage with messages you receive in connection with our Services, such as notifications to complete a learning activity.
        <br></br>
        <br></br>
        Your use of our Services.
        <br></br>
        <br></br>
        When you access or use our Services, we collect and store certain usage data. This includes the types of content you view or engage with, the frequency and duration of your activities, the scores you achieve in games and your progression through content. 
        <br></br>
        <br></br>
        Your networks and connections
        <br></br>
        <br></br>
        We collect information about the people and groups you are connected to and how you interact with them, such as the people you communicate with the most, or the frequency and types of team challenges you engage in. 
        <br></br>
        <br></br>
        Other users of our Services
        <br></br>
        <br></br>
        
        Other users of our Services may provide information about you when they create or share content through our Services. For example, you may be mentioned by someone else on their newsfeed.
        <br></br>
        <br></br>
        Websites and apps that use our Services
        <br></br>
        <br></br>
        We collect information when you visit or use third-party websites and apps that use our Services. This includes information about the websites and apps you visit, your use of our Services on those websites and apps, as well as information the developer or publisher of the app or website provides to you or us.
        <br></br>
        <br></br>
        Third-Party Services
        <br></br>
        <br></br>
        If you use a Third-Party Service (such as Google or Facebook) to register for an account, the Third-Party Service may provide us with your Third-Party Service account information on your behalf, such as your name and email address. Your privacy settings on the Third-Party Service normally control what they share with us. Make sure you are comfortable with what they share by reviewing their privacy policies and, if necessary, modifying your privacy settings directly on the Third-Party Service.
        <br></br>
        <br></br>
        Cogniss App Partners
        <br></br>
        <br></br>
        
        We may collect information about your interest in, and engagement with, our Services from Cogniss App Partners. Cogniss App Partners are individuals or organizations that build apps on the Cogniss platform. 
        
        
        
          </TabPanel>
            <TabPanel value={value} index={1}>
          

            <b className = {classes.heading}>2. How we share your information</b><br></br>
        
          <br></br>
          <br></br>

Our mission is to facilitate effective learning experiences that enrich people’s quality of life. We offer a platform on which developers, designers and others can imagine and create their own solutions to complex learning, health and behavioral challenges. 

With these objectives in mind, we use the personal information we collect about you to:
<br></br>
<br></br>

Provide and personalize our Services. We create and manage your account, and personalize our Services by using information about how you interact with them. For example, we may use your assessment results to provide you with customized insights to help you maintain or improve your learning. We may also use location information to incorporate location-specific challenges or help you locate relevant learning or wellness events near you.

Communicate with you. We communicate with you about our Services and let you know about any changes to our policies and terms. We also use your information to respond to your inquiries.
<br></br>
<br></br>
Improve our Services. We conduct surveys and research in order to test, evaluate, and improve existing products and services, or develop new ones. We may also conduct audits and troubleshooting activities.
<br></br>
<br></br>
Promote and advertise our Services. We may send you tailored marketing communications about products, services, and offers of Cogniss and our partners and measure the success of those campaigns. For example, we may promote a learning app to you based on what we think may interest you. We send these communications through email, notices posted on our websites or apps, and push notifications. You may opt out of receiving these notifications at any time by contacting us at privacy@cogniss.com. 
<br></br>
<br></br>
Promote safety and security. We help verify accounts and activities to promote safety and security on and off of our Services. This includes enforcing our Terms of Use and other legal terms and policies.
<br></br>
<br></br>
Aggregate insights. We use your data to produce and share aggregated insights that do not identify you. For example, we may use your data to generate statistics about our users, or to publish demographics for a Service or learning and behavioral insights.

<br></br>
<br></br>

We process your personal information for the above purposes on the following grounds:
<br></br>
<br></br>
<br></br>
Consent. You have consented to the use of your personal information in a particular way. When you consent, you can change your mind at any time by contacting us at privacy@cogniss.com.
<br></br>
<br></br>
<br></br>
Performance of a contract. We need your personal information to provide you with services and products requested by you, or to respond to your inquiries. In other words, so we can perform our contract with you, like the Terms of Use.
<br></br>
<br></br>
<br></br>
Legal obligation. We have a legal obligation to use your personal information, such as to comply with applicable tax and other government regulations or to comply with a court order or binding law enforcement request.
<br></br>
<br></br>
<br></br>
Legitimate interests. We have a legitimate interest in using your personal information in the following cases:
<br></br>
<br></br>
<br></br>
To operate the Cogniss business and provide you with tailored communications to assist you in better using Cogniss Services. This may include advertising; however, you may opt out of receiving advertising communications at any time by contacting us at privacy@cogniss.com or by following the unsubscribe instructions in any such communications. 
<br></br>
<br></br>
<br></br>
To analyze and improve the safety and security of our Services - we do this as it is necessary to pursue our legitimate interests in ensuring Cogniss is secure, such as by implementing and enhancing security measures and protections and protecting against fraud, spam and abuse.
<br></br>
<br></br>
<br></br>
To provide and improve our Services, including any personalized services - we do this as it is necessary to pursue our legitimate interests of providing an innovative and tailored offering to our Users on a sustained basis.
<br></br>
<br></br>
<br></br>
To anonymize and subsequently use anonymized information.
<br></br>
<br></br>
<br></br>
Protecting you and others. To protect your vital interests, or those of others. 
<br></br>
<br></br>
<br></br>
Others’ legitimate interests. Where necessary for the purposes of a third party’s legitimate interests, such as our Cogniss App Partners who have a legitimate interest in delivering tailored advertising to you and monitoring and measuring its effectiveness.  
<br></br>
<br></br>
<br></br>


Please note that, for technical reasons, there is likely to be a delay in deleting your personal information from our systems when you ask us to delete it.
            </TabPanel>
        </DialogContent>
        <div>
        <Checkbox inputProps={{ 'aria-label': 'uncontrolled-checkbox' }} onChange = {handleChange_checkbox} />
        I have read and agree to  the terms and privacy policy
      </div>

        
        <DialogActions>

      
        <Link to={'/register'}>

        <Button variant="contained" color = "primary" onClick={handleClose} disabled = {!checked}>
            Accept
          </Button></Link>
          <Button variant="contained" color = "primary" onClick={handleClose}>
            Close
          </Button>
        </DialogActions>
      </Dialog>





  </>
  );
}
export default Registration
