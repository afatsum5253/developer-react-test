import React from 'react'
import {Helmet} from "react-helmet"
export default ({title})=>{
  let name = 'Cogniss'
  if(title) return (<Helmet>
      <title>{name} | {title}</title>
  </Helmet>)
  return (<Helmet>
      <title>{name}</title>
  </Helmet>)
}
