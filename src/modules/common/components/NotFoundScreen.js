import React from 'react';
import {Link} from 'react-router-dom'
import styled from 'styled-components'

const Wrapper = styled.div `
  text-align:center;
`;
export default class Component extends React.Component{
  render(){
    return (
      <Wrapper>
        <h2>Not found</h2>
        <p><Link to="/">Go back to the homepage</Link></p>
      </Wrapper>
    )
  }
}
