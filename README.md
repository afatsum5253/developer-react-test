# Cogniss Developer Test

## Setup

- Bitbucket Account
- Node 12.9.0
- Git
- Any IDE of your choice

Please fork this repository (hit the + button on the side). Include your full name as part of the name of the fork. Clone your forked project to your PC and begin work.  
  
## Task Details
  
Your task is to create a popup, according to the design images in the *wireframes* folder. When the user clicks the "Create Account" button on the Login Screen, you should display the popup, which contains two text sections;  

- The Statement of Intent up the top, which is hidden by default but expands when you press it.
- The Terms of Use and the Privacy Policy, which exist on their own tabs and display different text.

The "Cancel" button should simply dismiss the popup. The "Accept" button should send the user to the Create Account screen. The "Accept" button should be locked/greyed out until the checkbox has been selected.  
  
The popup should be able to handle more Terms/Privacy Policy text than can fit on the screen (i.e. the scroll bar).  
  
## Guidelines

- Set yourself a limit of 2 hours maximum to complete the task. Try to complete as much as possible.
- You may hardcode in placeholder text for the three boxes.
- You may not modify any of the existing components, except to add navigation/popup functionality to the appropriate screen(s).
- You may create/add as many new components as you need to implement the popup.
- You may add any Node packages that you need to implement the popup.
- Smaller work-in-progress commits with messages are prefered to one large final commit.

You can launch the web application with the command `npm start`. Please make sure you run `npm install` first. The application watches for changes to the source, so you only need to start the application once, and begin work from there.

If you see a blank page after starting the application, simply refresh the page. If that does not work, check the browser's Console for error logs.

If you have any questions or issues, please don't hesitate to email me at [ben.steenson@2and2.com.au](mailto:ben.steenson@2and2.com.au).  
  
## Submission
  
Once you are confident with your work, please push any commits to your fork, and send an email to [ben.steenson@2and2.com.au](mailto:ben.steenson@2and2.com.au) to confirm you have completed the task.
